<section id="logos-proyecto">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/todoslogos.svg" alt="logos proyecto">
			</div>
		</div>
	</div>
</section>

<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<?php echo get_field('legal_footer') ?>
			</div>
		</div>
	</div>
</footer>

</body>
	<?php wp_footer() ?>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/validar.js"></script>
</html>