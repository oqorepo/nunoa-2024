<?php
//incluye wordpress para poder hacer el update del field que contiene el valor de la UF
require_once("wp-load.php");

//trae la data
$api_data = json_decode( file_get_contents('http://api.sbif.cl/api-sbifv3/recursos_api/uf?apikey=97705792b80fee545cae84b377509ae9da9935cb&formato=json'), true );
//define la variable valor_uf
$valor_uf = $api_data['UFs'][0]['Valor'];
//formatea el numero
$valor_uf = str_replace('.', '', $valor_uf);
$valor_uf = str_replace(',', '.', $valor_uf);

//update post field
update_post_meta( $post_id = 11, $key = 'valor_uf', $value = $valor_uf );
update_post_meta( $post_id = 112, $key = 'valor_uf', $value = $valor_uf );
?>