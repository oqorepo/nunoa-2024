//SLIDER TOP
var owlBannerHome = jQuery('#slider-top');
owlBannerHome.owlCarousel({
    items: 1,
    loop: true,
    margin: 0,
    nav: false,
    dots: true,
    autoplay: true,
    navText: ['<', '>'],
    autoplayTimeout: 4000
});
//SLIDER RENDERS
var owlRenders = jQuery('#renders');
owlRenders.owlCarousel({
    items: 1,
    loop: true,
    margin: 0,
    nav: true,
    dots: false,
    autoHeight: true,
    autoplay: false,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
});

//start tooltips
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

//CALCULADORA DE CREDITO HIPOTECARIO
//por Dario Biotti, Agencia OQO, Chile.

//FUNCIONES EXTERNAS
//darle formato a numeros.
var agregarPunto = function (valor) {
    var value = valor;
    value += '';
    x = value.split('.');
    x1 = x[0];
    // alert(x1);
    // x2 = x1.length > 1 ? ',' + x1[1] : '';
    // alert(x2);
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {

        x1 = x1.replace(rgx, '$1' + '.' + '$2');
        value = x1;
    }
    return value;
};
//función pmt de calculo.
function pmt(rate_per_period, number_of_payments, present_value, future_value, type) {
    future_value = typeof future_value !== 'undefined' ? future_value : 0;
    type = typeof type !== 'undefined' ? type : 0;
    if (rate_per_period != 0.0) {
        // Interest rate exists.
        var q = Math.pow(1 + rate_per_period, number_of_payments);
        return -(rate_per_period * (future_value + (q * present_value))) / ((-1 + q) * (1 + rate_per_period * (type)));
    } else if (number_of_payments != 0.0) {
        // No interest rate, but number of payments exists.
        return -(future_value + present_value) / number_of_payments;
    }
    return 0;
}

//VARIABLES

//trae el valor de la uf desde la api de miindicador.cl
var uf;

/*
$.ajax({
    url: 'http://api.sbif.cl/api-sbifv3/recursos_api/uf?apikey=97705792b80fee545cae84b377509ae9da9935cb&formato=json',
    dataType: 'json',
    async: false,
    success: function (data) {
        console.log(data.UFs[0].Valor);
        var uf2 = data.UFs[0].Valor;
        uf = parseInt(uf2.split('.').join("").replace(/,/g, '.'));
        console.log(uf);
    },
    error: function (ex) {
        uf = 28000;
        console.log("error");
    }
});*/



//setea las variables.
var cae = 3,
    ufm2renta = 0.25,
    años = 30,
    valorUF = 4155,
    pie = 20,
    pieUF = (pie / 100 * valorUF),
    pieCLP = pieUF * uf,
    m2util = 60.05,
    rap = ((m2util * ufm2renta * uf) * 12) / (1 / 100 * valorUF * uf),
    pmt1 = ((1 + (cae / 100)) ** (1 / 12)) - 1,
    pmt2 = años * 12,
    pmt3 = valorUF - (valorUF * (pie / 100)),
    dividendoUF = -pmt(pmt1, pmt2, pmt3),
    dividendo = dividendoUF * uf,
    raf = (((m2util * ufm2renta * uf) - dividendo) * 12) / ((pie / 10000) * valorUF * uf);

//funciones para actualizar las variables.

//linkeada al selector de plantas.
function setUfm2() {
    window.ufm2renta = document.getElementById('tipologia').selectedOptions[0].value;
    window.selectorUF = document.getElementById('tipologia').selectedOptions[0];
    window.m2util = selectorUF.getAttribute('data-m2util');
    window.valorUF = selectorUF.getAttribute('data-valoruf');
    window.plantaIMG = selectorUF.getAttribute('data-img');
    window.stotal = selectorUF.getAttribute('data-stotal');
    window.sterraza = selectorUF.getAttribute('data-sterraza');
    window.sinterior = selectorUF.getAttribute('data-sinterior');
    window.tipoP = selectorUF.getAttribute('data-tipo');
    window.programa = selectorUF.getAttribute('data-programa');
    setVariables();
}
//linkeada al input del %CAE.
function setCae() {
    window.cae = $('#cae').val();
    setVariables();
}
//linkeada al input de los años del credito.
function setAños() {
    window.años = $('#añosCredito').val();
    setVariables();
}
//linkeada al input del porcentaje del pie.
function setPie() {
    window.pie = $('#pieCredito').val();
    setVariables();
}

function setVariables() {
    window.pieUF = (pie / 100 * valorUF);
    window.pieCLP = pieUF * uf;
    window.pmt1 = ((1 + (cae / 100)) ** (1 / 12)) - 1;
    window.pmt2 = años * 12;
    window.pmt3 = valorUF - (valorUF * (pie / 100));
    window.dividendoUF = -pmt(pmt1, pmt2, pmt3);
    window.dividendo = dividendoUF * uf;
    window.rap = ((m2util * ufm2renta * uf) * 12) / (1 / 100 * valorUF * uf);
    window.raf = (((m2util * ufm2renta * uf) - dividendo) * 12) / ((pie / 10000) * valorUF * uf);
    $('#valoruf').html(agregarPunto(uf));
    $('#rentaExigida').html(agregarPunto(dividendo * 4));
    $('#rentaEstimada').html(agregarPunto(ufm2renta));
    $('#rentaEstimada2').html(agregarPunto(ufm2renta));
    $('#ufPie').html(agregarPunto(pieUF.toFixed(0)));
    $('#clpPie').html(agregarPunto(pieCLP));
    $('#dividendoMes').html(agregarPunto(dividendo));
    $('#rentaMes').html(agregarPunto(m2util * ufm2renta * uf));
    $('#rentaAnualPura').html(agregarPunto(rap.toFixed(2)));
    $('#rentaAnualFinanciada').html(agregarPunto(raf.toFixed(2)));
    $('#sc-img-planta').attr("src", plantaIMG);
    $('#sc-tipo-planta').html(tipoP);
    $('#sc-programa-planta').html(programa);
    $('#sc-stotal-planta').html(stotal + ' M2 Totales');
    $('#sc-sinterior-planta').html(sinterior + ' M2 Totales');
    $('#sc-sterraza-planta').html(sterraza + ' M2 Totales');
    $('#sc-valoruf').html('UF ' + agregarPunto(valorUF));
    $('#valorufdepto').html(agregarPunto(valorUF));
    $('#planta-cotizada').val(tipoP);
}


//SELECTORES
//resetea el selector de los años del credito al refrescar la página.
document.getElementById('añosCredito').options[2].selected = 'selected';

//inserta eventos onchange en los selectores.
/*
$('#tipologia').attr("onchange", "setUfm2(event)");
$('#pieCredito').attr("onchange", "setPie(event)");
$('#cae').attr("onchange", "setCae(event)");
$('#añosCredito').attr("onchange", "setAños(event)");
*/

//Imprimir plantas en el selector.
$.each(plantas, function (i, v) {
    if (v.tipo == 'Tipo A1') {
        $('#tipoa').append('<option data-valoruf="' + v.valorUF + '" data-programa="' + v.programa + '" name="' + v.tipo + '" data-tipo="' + v.tipo + '" data-sterraza="' + v.sterraza + '" data-sinterior="' + v.sinterior + '" data-stotal="' + v.stotal + '" data-img="' + v.foto + '" data-m2util="' + v.m2util + '" value="' + v.ufm2renta + '">' + v.tipo + '</option>');
    };
    if (v.tipo == 'Tipo B1' || v.tipo == "Tipo B1'" || v.tipo == 'Tipo B2') {
        $('#tipob').append('<option data-valoruf="' + v.valorUF + '" data-programa="' + v.programa + '" name="' + v.tipo + '" data-tipo="' + v.tipo + '" data-sterraza="' + v.sterraza + '" data-sinterior="' + v.sinterior + '" data-stotal="' + v.stotal + '" data-img="' + v.foto + '" data-m2util="' + v.m2util + '" value="' + v.ufm2renta + '">' + v.tipo + '</option>');
    };
    if (v.tipo == 'Tipo C1' || v.tipo == "Tipo C1'" || v.tipo == 'Tipo C2') {
        $('#tipoc').append('<option data-valoruf="' + v.valorUF + '" data-programa="' + v.programa + '" name="' + v.tipo + '" data-tipo="' + v.tipo + '" data-sterraza="' + v.sterraza + '" data-sinterior="' + v.sinterior + '" data-stotal="' + v.stotal + '" data-img="' + v.foto + '" data-m2util="' + v.m2util + '" value="' + v.ufm2renta + '">' + v.tipo + '</option>');
    };
    if (v.tipo == 'Tipo D' || v.tipo == "Tipo D1" || v.tipo == 'Tipo D3') {
        $('#tipod').append('<option data-valoruf="' + v.valorUF + '" data-programa="' + v.programa + '" name="' + v.tipo + '" data-tipo="' + v.tipo + '" data-sterraza="' + v.sterraza + '" data-sinterior="' + v.sinterior + '" data-stotal="' + v.stotal + '" data-img="' + v.foto + '" data-m2util="' + v.m2util + '" value="' + v.ufm2renta + '">' + v.tipo + '</option>');
    };
});

//Tipologia de inicio
window.m2util = document.getElementById('tipologia').selectedOptions[0].getAttribute('data-m2util');
window.valorUF = document.getElementById('tipologia').selectedOptions[0].getAttribute('data-valoruf');
window.plantaIMG = document.getElementById('tipologia').selectedOptions[0].getAttribute('data-img');
window.stotal = document.getElementById('tipologia').selectedOptions[0].getAttribute('data-stotal');
window.sterraza = document.getElementById('tipologia').selectedOptions[0].getAttribute('data-sterraza');
window.sinterior = document.getElementById('tipologia').selectedOptions[0].getAttribute('data-sinterior');
window.tipoP = document.getElementById('tipologia').selectedOptions[0].getAttribute('data-tipo');
window.programa = document.getElementById('tipologia').selectedOptions[0].getAttribute('data-programa');

//inicializa selectores.
$('.selectpicker').selectpicker();
$('.selectpicker2').selectpicker();

//Imprime las variables iniciales.
$('#valoruf').html(agregarPunto(uf));
$('#rentaExigida').html(agregarPunto(dividendo * 4));
$('#rentaEstimada').html(agregarPunto(ufm2renta));
$('#rentaEstimada2').html(agregarPunto(ufm2renta));
$('#ufPie').html(agregarPunto(pieUF.toFixed(0)));
$('#clpPie').html(agregarPunto(pieCLP));
$('#dividendoMes').html(agregarPunto(dividendo));
$('#rentaMes').html(agregarPunto(m2util * ufm2renta * uf));
$('#rentaAnualPura').html(agregarPunto(rap.toFixed(2)));
$('#rentaAnualFinanciada').html(agregarPunto(raf.toFixed(2)));
$('#sc-img-planta').attr("src", plantaIMG);
$('#sc-tipo-planta').html(tipoP);
$('#sc-programa-planta').html(programa);
$('#sc-stotal-planta').html(stotal + ' M2 Total');
$('#sc-sinterior-planta').html(sinterior + ' M2 Total');
$('#sc-sterraza-planta').html(sterraza + ' M2 Total');
$('#sc-valoruf').html('UF ' + agregarPunto(valorUF));
$('#valorufdepto').html(agregarPunto(valorUF));
$('#planta-cotizada').val(tipoP);





//SIMULADOR DOS
//VARIABLES

//setea las variables.
var cae4 = 3,
    ufm2renta4 = 0.25,
    años4 = 30,
    valorUF4 = 4155,
    pie4 = 20,
    pieUF4 = (pie4 / 100 * valorUF4),
    pieCLP4 = pieUF4 * uf,
    m2util4 = 60.05,
    rap4 = ((m2util4 * ufm2renta4 * uf) * 12) / (1 / 100 * valorUF4 * uf),
    pmt14 = ((1 + (cae4 / 100)) ** (1 / 12)) - 1,
    pmt24 = años4 * 12,
    pmt34 = valorUF4 - (valorUF4 * (pie4 / 100)),
    dividendoUF4 = -pmt(pmt14, pmt24, pmt34),
    dividendo4 = dividendoUF4 * uf,
    raf4 = (((m2util4 * ufm2renta4 * uf) - dividendo4) * 12) / ((pie4 / 10000) * valorUF4 * uf);

//funciones para actualizar las variables.

//linkeada al selector de plantas.
function setUfm24() {
    window.ufm2renta4 = document.getElementById('tipologia4').selectedOptions[0].value;
    window.selectorUF4 = document.getElementById('tipologia4').selectedOptions[0];
    window.m2util4 = selectorUF4.getAttribute('data-m2util');
    window.valorUF4 = selectorUF4.getAttribute('data-valoruf');
    window.plantaIMG4 = selectorUF4.getAttribute('data-img');
    window.stotal4 = selectorUF4.getAttribute('data-stotal');
    window.sterraza4 = selectorUF4.getAttribute('data-sterraza');
    window.sinterior4 = selectorUF4.getAttribute('data-sinterior');
    window.tipoP4 = selectorUF4.getAttribute('data-tipo');
    window.programa4 = selectorUF4.getAttribute('data-programa');
    setVariables4();
}
//linkeada al input del %CAE.
function setCae4() {
    window.cae4 = $('#cae4').val();
    setVariables4();
}
//linkeada al input de los años del credito.
function setAños4() {
    window.años4 = $('#añosCredito4').val();
    setVariables4();
}
//linkeada al input del porcentaje del pie.
function setPie4() {
    window.pie4 = $('#pieCredito4').val();
    setVariables4();
}

function setVariables4() {
    window.pieUF4 = (pie4 / 100 * valorUF4);
    window.pieCLP4 = pieUF4 * uf;
    window.pmt14 = ((1 + (cae4 / 100)) ** (1 / 12)) - 1;
    window.pmt24 = años4 * 12;
    window.pmt34 = valorUF4 - (valorUF4 * (pie4 / 100));
    window.dividendoUF4 = -pmt(pmt14, pmt24, pmt34);
    window.dividendo4 = dividendoUF4 * uf;
    window.rap4 = ((m2util4 * ufm2renta4 * uf) * 12) / (1 / 100 * valorUF4 * uf);
    window.raf4 = (((m2util4 * ufm2renta * uf) - dividendo4) * 12) / ((pie4 / 10000) * valorUF4 * uf);
    $('#valoruf4').html(agregarPunto(uf));
    $('#rentaExigida4').html(agregarPunto(dividendo4 * 4));
    $('#rentaEstimada4').html(agregarPunto(ufm2renta4));
    $('#rentaEstimada24').html(agregarPunto(ufm2renta4));
    $('#ufPie4').html(agregarPunto(pieUF4.toFixed(0)));
    $('#clpPie4').html(agregarPunto(pieCLP4));
    $('#dividendoMes4').html(agregarPunto(dividendo4));
    $('#rentaMes4').html(agregarPunto(m2util4 * ufm2renta4 * uf));
    $('#rentaAnualPura4').html(agregarPunto(rap4.toFixed(2)));
    $('#rentaAnualFinanciada4').html(agregarPunto(raf4.toFixed(2)));
    $('#sc-img-planta4').attr("src", plantaIMG4);
    $('#sc-tipo-planta4').html(tipoP4);
    $('#sc-programa-planta4').html(programa4);
    $('#sc-stotal-planta4').html(stotal4 + ' M2 Totales');
    $('#sc-sinterior-planta4').html(sinterior4 + ' M2 Totales');
    $('#sc-sterraza-planta4').html(sterraza4 + ' M2 Totales');
    $('#sc-valoruf4').html('UF ' + agregarPunto(valorUF4));
    $('#valorufdepto4').html(agregarPunto(valorUF4));
    $('#planta-cotizada4').val(tipoP4);
}


//SELECTORES
//resetea el selector de los años del credito al refrescar la página.
document.getElementById('añosCredito4').options[2].selected = 'selected';

//inserta eventos onchange en los selectores.
/*
$('#tipologia4').attr("onchange", "setUfm24(event)");
$('#pieCredito4').attr("onchange", "setPie4(event)");
$('#cae4').attr("onchange", "setCae4(event)");
$('#añosCredito4').attr("onchange", "setAños4(event)");
*/

//Imprimir plantas en el selector.
$.each(plantas, function (i, v) {
    if (v.tipo == 'Tipo A1') {
        $('#tipoa4').append('<option data-valoruf="' + v.valorUF + '" data-programa="' + v.programa + '" name="' + v.tipo + '" data-tipo="' + v.tipo + '" data-sterraza="' + v.sterraza + '" data-sinterior="' + v.sinterior + '" data-stotal="' + v.stotal + '" data-img="' + v.foto + '" data-m2util="' + v.m2util + '" value="' + v.ufm2renta + '">' + v.tipo + '</option>');
    };
    if (v.tipo == 'Tipo B1' || v.tipo == "Tipo B1'" || v.tipo == 'Tipo B2') {
        $('#tipob4').append('<option data-valoruf="' + v.valorUF + '" data-programa="' + v.programa + '" name="' + v.tipo + '" data-tipo="' + v.tipo + '" data-sterraza="' + v.sterraza + '" data-sinterior="' + v.sinterior + '" data-stotal="' + v.stotal + '" data-img="' + v.foto + '" data-m2util="' + v.m2util + '" value="' + v.ufm2renta + '">' + v.tipo + '</option>');
    };
    if (v.tipo == 'Tipo C1' || v.tipo == "Tipo C1'" || v.tipo == 'Tipo C2') {
        $('#tipoc4').append('<option data-valoruf="' + v.valorUF + '" data-programa="' + v.programa + '" name="' + v.tipo + '" data-tipo="' + v.tipo + '" data-sterraza="' + v.sterraza + '" data-sinterior="' + v.sinterior + '" data-stotal="' + v.stotal + '" data-img="' + v.foto + '" data-m2util="' + v.m2util + '" value="' + v.ufm2renta + '">' + v.tipo + '</option>');
    };
    if (v.tipo == 'Tipo D' || v.tipo == "Tipo D1" || v.tipo == 'Tipo D3') {
        $('#tipod4').append('<option data-valoruf="' + v.valorUF + '" data-programa="' + v.programa + '" name="' + v.tipo + '" data-tipo="' + v.tipo + '" data-sterraza="' + v.sterraza + '" data-sinterior="' + v.sinterior + '" data-stotal="' + v.stotal + '" data-img="' + v.foto + '" data-m2util="' + v.m2util + '" value="' + v.ufm2renta + '">' + v.tipo + '</option>');
    };
});

//Tipologia de inicio
document.getElementById('tipologia4').options[0].selected = 'selected';$('#tipologia4').selectpicker('refresh');
window.m2util4 = document.getElementById('tipologia4').selectedOptions[0].getAttribute('data-m2util');
window.valorUF4 = document.getElementById('tipologia4').selectedOptions[0].getAttribute('data-valoruf');
window.plantaIMG4 = document.getElementById('tipologia4').selectedOptions[0].getAttribute('data-img');
window.stotal4 = document.getElementById('tipologia4').selectedOptions[0].getAttribute('data-stotal');
window.sterraza4 = document.getElementById('tipologia4').selectedOptions[0].getAttribute('data-sterraza');
window.sinterior4 = document.getElementById('tipologia4').selectedOptions[0].getAttribute('data-sinterior');
window.tipoP4 = document.getElementById('tipologia4').selectedOptions[0].getAttribute('data-tipo');
window.programa4 = document.getElementById('tipologia4').selectedOptions[0].getAttribute('data-programa');

//inicializa selectores.
$('.selectpicker4').selectpicker();
$('.selectpicker24').selectpicker();

//Imprime las variables iniciales.
$('#valoruf4').html(agregarPunto(uf));
$('#rentaExigida4').html(agregarPunto(dividendo4 * 4));
$('#rentaEstimada4').html(agregarPunto(ufm2renta4));
$('#rentaEstimada24').html(agregarPunto(ufm2renta4));
$('#ufPie4').html(agregarPunto(pieUF4.toFixed(0)));
$('#clpPie4').html(agregarPunto(pieCLP4));
$('#dividendoMes4').html(agregarPunto(dividendo4));
$('#rentaMes4').html(agregarPunto(m2util4 * ufm2renta4 * uf));
$('#rentaAnualPura4').html(agregarPunto(rap4.toFixed(2)));
$('#rentaAnualFinanciada4').html(agregarPunto(raf4.toFixed(2)));
$('#sc-img-planta4').attr("src", plantaIMG4);
$('#sc-tipo-planta4').html(tipoP4);
$('#sc-programa-planta4').html(programa4);
$('#sc-stotal-planta4').html(stotal4 + ' M2 Total');
$('#sc-sinterior-planta4').html(sinterior4 + ' M2 Total');
$('#sc-sterraza-planta4').html(sterraza4 + ' M2 Total');
$('#sc-valoruf4').html('UF ' + agregarPunto(valorUF4));
$('#valorufdepto4').html(agregarPunto(valorUF4));
$('#planta-cotizada4').val(tipoP4);

//flip card
if ($(window).width() < 560) {
    $('.flip').click(function(){
        $(this).find('.card').toggleClass('flipped');
    });
}
else {
    $(".card").hover(
        function () {
            $(this).addClass("flipped");
        },
        function () {
            $(this).removeClass("flipped");
        }
    );
}


//botones para simular
$('#btn-simula').click(function(){
    actualizarDatos();
});
$('#btn-simula-2').click(function(){
    actualizarDatos2();
});

function actualizarDatos2() {
    setUfm24();
    setCae4();
    setPie4();
    setAños4();
};
function actualizarDatos() {
    setUfm2();
    setCae();
    setPie();
    setAños();
};

//cotizar antes de simular de nuevo

var entregoDatos = 0;
var simulaciones = 0;
var entregoDatos = localStorage.getItem('entregoDatos');
if (entregoDatos == "1") {
    
} else {
    $('#btn-simula').click(function(){
        simulaciones = simulaciones + 1;
        if (simulaciones >= 3) {
            $('#valoruf4').html('0');
            $('#rentaExigida4').html('0');
            $('#rentaEstimada4').html('0');
            $('#rentaEstimada24').html('0');
            $('#ufPie4').html('0');
            $('#clpPie4').html('0');
            $('#dividendoMes4').html('0');
            $('#rentaMes4').html('0');
            $('#rentaAnualPura4').html('0');
            $('#rentaAnualFinanciada4').html('0');
            $('#valoruf').html('0');
            $('#rentaExigida').html('0');
            $('#rentaEstimada').html('0');
            $('#rentaEstimada2').html('0');
            $('#ufPie').html('0');
            $('#clpPie').html('0');
            $('#dividendoMes').html('0');
            $('#rentaMes').html('0');
            $('#rentaAnualPura').html('0');
            $('#rentaAnualFinanciada').html('0');
            $('#modalCotizarForce').modal('show');
        }
    });
    $('#btn-simula-2').click(function(){
        simulaciones = simulaciones + 1;
        if (simulaciones >= 3) {
            $('#valoruf4').html('0');
            $('#rentaExigida4').html('0');
            $('#rentaEstimada4').html('0');
            $('#rentaEstimada24').html('0');
            $('#ufPie4').html('0');
            $('#clpPie4').html('0');
            $('#dividendoMes4').html('0');
            $('#rentaMes4').html('0');
            $('#rentaAnualPura4').html('0');
            $('#rentaAnualFinanciada4').html('0');
            $('#valoruf').html('0');
            $('#rentaExigida').html('0');
            $('#rentaEstimada').html('0');
            $('#rentaEstimada2').html('0');
            $('#ufPie').html('0');
            $('#clpPie').html('0');
            $('#dividendoMes').html('0');
            $('#rentaMes').html('0');
            $('#rentaAnualPura').html('0');
            $('#rentaAnualFinanciada').html('0');
            $('#modalCotizarForce2').modal('show');
        }
    });
}

document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '134' == event.detail.contactFormId ) {
        localStorage.setItem('entregoDatos', '1');
        simulado = localStorage.getItem('entregoDatos');
        setTimeout(function() { location.reload(); }, 2000);  
    }
}, false );

/*$('.wpcf7-form').on('wpcf7mailsent', function(e){
    localStorage.setItem('entregoDatos', '1');
    simulado = localStorage.getItem('entregoDatos');
    setTimeout(function() { location.reload(); }, 2000);    
});*/

$('.sc-inputs input').change(function() {
    $('#valorufdepto4').html('-');
    $('#valoruf4').html('-');
    $('#rentaExigida4').html('-');
    $('#rentaEstimada4').html('-');
    $('#rentaEstimada24').html('-');
    $('#ufPie4').html('-');
    $('#clpPie4').html('-');
    $('#dividendoMes4').html('-');
    $('#rentaMes4').html('-');
    $('#rentaAnualPura4').html('-');
    $('#rentaAnualFinanciada4').html('-');
    $('#valorufdepto').html('-');
    $('#valoruf').html('-');
    $('#rentaExigida').html('-');
    $('#rentaEstimada').html('-');
    $('#rentaEstimada2').html('-');
    $('#ufPie').html('-');
    $('#clpPie').html('-');
    $('#dividendoMes').html('-');
    $('#rentaMes').html('-');
    $('#rentaAnualPura').html('-');
    $('#rentaAnualFinanciada').html('-');
});
$('#tipologia4').change(function() {
    $('#valorufdepto4').html('-');
    $('#valoruf4').html('-');
    $('#rentaExigida4').html('-');
    $('#rentaEstimada4').html('-');
    $('#rentaEstimada24').html('-');
    $('#ufPie4').html('-');
    $('#clpPie4').html('-');
    $('#dividendoMes4').html('-');
    $('#rentaMes4').html('-');
    $('#rentaAnualPura4').html('-');
    $('#rentaAnualFinanciada4').html('-');
});
$('#añosCredito4').change(function() {
    $('#valorufdepto4').html('-');
    $('#valoruf4').html('-');
    $('#rentaExigida4').html('-');
    $('#rentaEstimada4').html('-');
    $('#rentaEstimada24').html('-');
    $('#ufPie4').html('-');
    $('#clpPie4').html('-');
    $('#dividendoMes4').html('-');
    $('#rentaMes4').html('-');
    $('#rentaAnualPura4').html('-');
    $('#rentaAnualFinanciada4').html('-');
});
$('#tipologia').change(function() {
    $('#valorufdepto').html('-');
    $('#valoruf').html('-');
    $('#rentaExigida').html('-');
    $('#rentaEstimada').html('-');
    $('#rentaEstimada2').html('-');
    $('#ufPie').html('-');
    $('#clpPie').html('-');
    $('#dividendoMes').html('-');
    $('#rentaMes').html('-');
    $('#rentaAnualPura').html('-');
    $('#rentaAnualFinanciada').html('-');
});
$('#añosCredito').change(function() {
    $('#valorufdepto').html('-');
    $('#valoruf').html('-');
    $('#rentaExigida').html('-');
    $('#rentaEstimada').html('-');
    $('#rentaEstimada2').html('-');
    $('#ufPie').html('-');
    $('#clpPie').html('-');
    $('#dividendoMes').html('-');
    $('#rentaMes').html('-');
    $('#rentaAnualPura').html('-');
    $('#rentaAnualFinanciada').html('-');
});