<?php /* Template Name: Homepage new */ ?>
<?php get_header(); ?>

<section id="banner-top" class="container-fluid">
	<div class="row">
		<div class="slider-top">
			<div id="slider-top" class="owl-carousel owl-theme">
				<?php
			    //Get the images ids from the post_metadata
			    $images = acf_photo_gallery('slider-top', $post->ID);
			    //Check if return array has anything in it
			    if( count($images) ):
			        //Cool, we got some data so now let's loop over it
			        foreach($images as $image):
			            $id = $image['id']; // The attachment id of the media
			            $title = $image['title']; //The title
			            $caption= $image['caption']; //The caption
			            $full_image_url= $image['full_image_url']; //Full size image url
			            $thumbnail_image_url= $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
			            $url= $image['url']; //Goto any link when clicked
			            $target= $image['target']; //Open normal or new tab
			            $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
			            $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
				?>
					<img src="<?php echo $full_image_url; ?>" alt="">
				<?php endforeach; endif; ?>
			</div>
		</div>
	</div>
</section>

<section id="porque-invertir">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 title">
				<h1>¿Por qué invertir en un inmueble?</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 inv-txt">
				En una ciudad cada vez más densificada y con alta escasez de terrenos, 
				los precios de los Inmuebles son cada día mayores, creando grandes oportunidades de negocio para personas como tú.<br>
				Invierte en tu futuro.</div>
		</div>
		<div class="row icons">
			<div class="col-sm-offset-2 col-sm-2 icon">
				<div class="icon-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-02.svg" alt="">
				</div>
				<div class="icon-txt">
					Bajo capital de inversión
				</div>
			</div>
			<div class="col-sm-2 icon">
				<div class="icon-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-01.svg" alt="">
				</div>
				<div class="icon-txt">
					Alta plusvalía de la comuna
				</div>
			</div>
			<div class="col-sm-2 icon">
				<div class="icon-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-04.svg" alt="">
				</div>
				<div class="icon-txt">
					Compra en blanco a mejores precios
				</div>
			</div>
			<div class="col-sm-2 icon">
				<div class="icon-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-05.svg" alt="">
				</div>
				<div class="icon-txt">
					Aumenta tu patrimonio
				</div>
			</div>
		</div>
		<div class="row icons">
			<div class="col-sm-offset-3 col-sm-2 icon">
				<div class="icon-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-06.svg" alt="">
				</div>
				<div class="icon-txt">
					Complemento a tu jubilación
				</div>
			</div>
			<div class="col-sm-2 icon">
				<div class="icon-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-07.svg" alt="">
				</div>
				<div class="icon-txt">
					Otra manera de ahorrar
				</div>
			</div>
			<div class="col-sm-2 icon">
				<div class="icon-img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/icon-03.svg" alt="">
				</div>
				<div class="icon-txt">
					Rentabiliza tu inversión
				</div>
			</div>
		</div>
	</div>
</section>

<section id="conoce">
	<div class="container">
		<div class="row logo-title">
			<div class="col-xs-5 col-sm-5 lwt-left">
				<h1>Por qué </h1>
			</div>
			<div class="col-xs-7 col-sm-7 lwt-right">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/logo.svg" alt="logo proyecto">
			</div>
		</div>
	</div>
</section>

<section id="beneficios">
	<div class="container">
		<div class="row">
			<h2>Un buen departamento, siempre atrae a un buen arrendatario</h2><br>
			Ubicado en la calle Rodrigo de Araya, el Edificio Ñuñoa 2024 se caracteriza por su gran conectividad, por sus excelentes y numerosas áreas comunes, y por su gran diseño arquitectónico, convirtiéndolo en un atractivo lugar para vivir.
		</div>
		<div class="row">
			<?php
			    //Get the images ids from the post_metadata
			    $images = acf_photo_gallery('caracteristicas_edificio', $post->ID);
			    //Check if return array has anything in it
			    if( count($images) ):
			        //Cool, we got some data so now let's loop over it
			        foreach($images as $image):
			            $id = $image['id']; // The attachment id of the media
			            $title = $image['title']; //The title
			            $caption= $image['caption']; //The caption
			            $full_image_url= $image['full_image_url']; //Full size image url
			            $thumbnail_image_url= $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
			            $url= $image['url']; //Goto any link when clicked
			            $target= $image['target']; //Open normal or new tab
			            $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
			            $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
				?>
					<div class="col-sm-4 no-padding">
						<div class="caracteristica">
							<div class="caract-img">
								<img src="<?php echo $full_image_url; ?>" alt="">
								<div class="caract-txt">
										<?php echo $title; ?>
								</div>
							</div>
						</div>
					</div>
			<?php endforeach; endif; ?>
			
		</div>
		<div class="row">
			<div class="col-sm-12 beneficios-txt">
				Con 4 tipologías de departamentos, con excelentes terminaciones y gran diseño arquitectónico, los departamentos desde 1 hasta 2.5 dormitorios de 47.7 m2 a 64.6 m2 totales, son un gran atractivo para arrendatarios que buscan vivir esta gran experiencia.
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<a href="#plantas">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/project-btn.jpg" alt="">
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<h1 style="text-align: center">Ahora elige tu inversión</h1>
			</div>
		</div>
	</div>
</section>

<section id="plantas">
	<div class="container grey-bg">
		<div class="row">
			<?php
				$loop = new WP_Query( array(
    				'post_type' => 'plantas',
    				'posts_per_page' => -1
  					)
				);
			?>

			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="col-sm-6 planta">
				<a data-toggle="modal" data-target="#<?php echo get_field( "modalid" ); ?>" class="btn-planta">
					<ul>
						<li style="margin-bottom: 10px">
							<strong><?php echo get_field( "tipo_de_planta" ); ?><br>
							<?php echo get_field( "programa" ); ?><br></strong>
							<?php echo get_field( "superficie_total" ); ?> M2
						</li>
						<img src="<?php echo get_field( "imagen_planta" ); ?>" alt="">
					</ul>
				</a>
				
			</div>
			<div class="modal fade" id="<?php echo get_field( "modalid" ); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-right: 0px">
				<div class="modal-dialog" role="document">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-offset-1 col-sm-6 planta-img">
								<img src="<?php echo get_field( "imagen_planta" ); ?>" alt="">
							</div>
							<div class="col-sm-3 planta-info">
								<div class="bottom-line">
									<section id="simulador-credito">
    <div class="container">
        <div class="row">
        	<div class="col-sm-12 title">
				<h1>Simula un credito hipotecario</h1>
			</div>
            <div class="col-sm-offset-1 col-sm-4 sc-inputs">
                <p class="sc-label">
                    Departamento:
                </p>
                <select class="selectpicker" id="tipologia"></select><br>
                <p class="sc-label">
                    Porcentaje del pie:
                </p>
                <input id="pieCredito" type="text" value="20"><br>
                <p class="sc-label">
                    Tasa anual bco. y otros cobros (CAE%):
                </p>
                <input id="cae" type="text" value="3.5"><br>
                <p class="sc-label">
                    Departamento:
                </p>
                <select class="selectpicker2" id="añosCredito">
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                    <option value="30">30</option>
                    <option value="35">35</option>
                </select><br>
            </div>
            <div class="col-sm-6 sc-results">
            	<table class="tablaSC">
					<tbody>
					<tr>
					<td>Valor uf hoy: </td><td>$<span id="valoruf"></span></td></tr>
					<tr>
					<td>Renta estimada uf/m2:</td><td><span id="rentaEstimada"></span></td></tr>
					</tbody>
				</table>
				<br>
                <table class="tablaSC">
					<tbody>
					<tr>
					<td>UF: </td><td><span id="ufPie"></span></td></tr>
					<tr>
					<td>CLP: </td><td>$<span id="clpPie"></span></td></tr>
					</tbody>
				</table>
				<br>
                <table class="tablaSC">
					<tbody>
					<tr>
					<td>Renta liquida aprox exigida por el banco: </td><td>$<span id="rentaExigida"></span></td></tr>
					</tbody>
				</table>
				<br>
				<table class="tablaSC">
					<tbody>
					<tr>
					<td>Valor renta mes: </td><td>$<span id="rentaMes"></span></td></tr>
					<tr>
					<td>Dividendo mes: </td><td>$<span id="dividendoMes"></span></td></tr>
					<tr>
					<td>Renta anual pura: </td><td><span id="rentaAnualPura"></span>%</td></tr>
					<tr>
					<td>Renta anual financiada: </td><td><span id="rentaAnualFinanciada"></span>%</td></tr>
					</tbody>
				</table>
                <!--renta uf/m2: <span id="rentaEstimada2"></span><br>-->
            </div>
        </div>
    </div>
</section>
									<div class="planta-title">
										<div class="tipo-planta">
											<?php echo get_field( "tipo_de_planta" ); ?>
										</div>
										<div class="programa">
											1 Dormitorio + 1 baño
										</div>
									</div>
									
									<div class="planta-metros">
										<div class="sup01">
											<div class="m-title">Superficie interior</div>
											<div class="m-number"><?php echo get_field( "superficie_interior" ); ?> M2</div>
										</div>
										<div class="sup02">
											<div class="m-title">Terraza</div>
											<div class="m-number"><?php echo get_field( "superficie_terraza" ); ?> M2</div>
										</div>
										<div class="sup03">
											<div class="m-title">Superficie total</div>
											<div class="m-number"><?php echo get_field( "superficie_total" ); ?> M2</div>
										</div>
									</div>

									<div class="planta-orientacion">
										<!--<div class="orientacion">
											<img src="<?php echo get_field( "imagen_orientacion" ); ?>" alt="">
										</div>-->
										<div class="piso">
											<?php echo get_field( "piso" ); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
<?php endwhile; wp_reset_query(); ?>
		</div>
	</div>
</section>

<section id="conoce">
	<div class="container grey-bg">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 conoce-txt">
					Sabías que en Ñuñoa 2024 las viviendas son DFL2, lo que significa que se acogen a ciertos beneficios, tales como:
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<ul class="l01">
					<li>Están exentas de pagar el 50% de las contribuciones por 20 años.</li>
					<li>Rentas libres de impuesto.</li>
					<li>Libre de impuestos a la herencia o donaciones.</li>
					<li>Libre de impuestos a la renta por venta a mayor valor que el valor de compra.</li>
					<li>50% de descuento en el arancel del Conservador de Bienes Raíces.</li>
					<li>Reducción de la tasa de timbres y estampillas.</li>
				</ul>
				<img style="margin-bottom:-70px" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/orange-bar.png" alt="">
			</div>
		</div>
	</div>
</section>

<section id="simulador-credito">
    <div class="container">
        <div class="row">
        	<div class="col-sm-12 title">
				<h1>Simula un credito hipotecario</h1>
			</div>
            <div class="col-sm-offset-1 col-sm-4 sc-inputs">
                <p class="sc-label">
                    Departamento:
                </p>
                <select class="selectpicker" id="tipologia"></select><br>
                <p class="sc-label">
                    Porcentaje del pie:
                </p>
                <input id="pieCredito" type="text" value="20"><br>
                <p class="sc-label">
                    Tasa anual bco. y otros cobros (CAE%):
                </p>
                <input id="cae" type="text" value="3.5"><br>
                <p class="sc-label">
                    Departamento:
                </p>
                <select class="selectpicker2" id="añosCredito">
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                    <option value="30">30</option>
                    <option value="35">35</option>
                </select><br>
            </div>
            <div class="col-sm-6 sc-results">
            	<table class="tablaSC">
					<tbody>
					<tr>
					<td>Valor uf hoy: </td><td>$<span id="valoruf"></span></td></tr>
					<tr>
					<td>Renta estimada uf/m2:</td><td><span id="rentaEstimada"></span></td></tr>
					</tbody>
				</table>
				<br>
                <table class="tablaSC">
					<tbody>
					<tr>
					<td>UF: </td><td><span id="ufPie"></span></td></tr>
					<tr>
					<td>CLP: </td><td>$<span id="clpPie"></span></td></tr>
					</tbody>
				</table>
				<br>
                <table class="tablaSC">
					<tbody>
					<tr>
					<td>Renta liquida aprox exigida por el banco: </td><td>$<span id="rentaExigida"></span></td></tr>
					</tbody>
				</table>
				<br>
				<table class="tablaSC">
					<tbody>
					<tr>
					<td>Valor renta mes: </td><td>$<span id="rentaMes"></span></td></tr>
					<tr>
					<td>Dividendo mes: </td><td>$<span id="dividendoMes"></span></td></tr>
					<tr>
					<td>Renta anual pura: </td><td><span id="rentaAnualPura"></span>%</td></tr>
					<tr>
					<td>Renta anual financiada: </td><td><span id="rentaAnualFinanciada"></span>%</td></tr>
					</tbody>
				</table>
                <!--renta uf/m2: <span id="rentaEstimada2"></span><br>-->
            </div>
        </div>
    </div>
</section>

<section id="contactanos">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>Ahora que sabes porque es bueno invertir en nuestro proyecto, contáctanos</h1>
			</div>
			<div class="col-sm-12">
				Porque tu confianza es importante para nosotros es que te queremos acompañar y asesorar en este gran proceso, para que la decisión que tomes sea una decisión informada.<br>
				Rellena el siguiente formulario y nos pondremos en contacto contigo para asesorarte y responder a cualquier pregunta que tengas.<br>
				Cuenta con nosotros para lo que necesites.
			</div>
		</div>
	</div>
</section>

<section id="contacto">
	<div class="container contacto-bg">
		<div class="row">
			<div class="col-sm-5">
				<a href="https://www.google.cl/maps/place/Francisco+de+Paula+%26+Rodrigo+de+Araya,+%C3%91u%C3%B1oa,+Regi%C3%B3n+Metropolitana/@-33.4743846,-70.6144043,17z/data=!3m1!4b1!4m5!3m4!1s0x9662cffafc7f6987:0x2c7d7034078c3219!8m2!3d-33.4743846!4d-70.6122156">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/mapa.jpg" alt="" class="img-map">
				</a>
			</div>
			<div class="col-sm-7">
				<form method="post" action="<?php echo home_url() ?>/send.php">
					<div class="form-50">
						<input class="campo" type="hidden" name="correo-proyecto" value="<?php echo get_field('correo_proyecto') ?>" required="">
						Nombre<br>
						<input class="campo" type="text" name="nombre" required=""><br>
						Apellido<br>
						<input class="campo" type="text" name="apellido" required=""><br>
						Rut<br>
						<input class="campo" type="text" name="rut" required=""><br>
						E-Mail<br>
						<input class="campo" type="text" name="email" required=""><br>
					</div>
					<div class="form-50">
						Teléfono<br>
						<input class="campo" type="text" name="telefono" required=""><br>
						Escribe tu mensaje aquí<br>
						<textarea class="campo" cols="40" rows="7" name="mensaje"></textarea>
					</div>
					<div class="submit-wrap">
						<input type="submit" name="enviar" value="ENVIAR" class="inputsubmit"><br>
						<h2><?php echo get_field('telefono') ?></h2>
						<h2><?php echo get_field('correo-proyecto') ?></h2>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<?php get_footer() ?>