<!DOCTYPE html>
<html>
<head>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVJ4SHX');</script>
<!-- End Google Tag Manager -->
	<title>
        <?php echo get_bloginfo('name');?> | <?php echo get_bloginfo('description');?>
    </title>
	<meta charset="utf-8" />
	<meta name="google-site-verification" content="r84nB3eGiRJmXbcoVRz0Exa6dJeUloV5EpYPyoICkt0" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<?php wp_head(); ?>
	<link rel="stylesheet" href='<?php echo get_template_directory_uri(); ?>/assets/css/plugins.css' type='text/css'>
	<link rel="stylesheet" href='<?php echo get_template_directory_uri(); ?>/assets/fonts/stylesheet.css' type='text/css'/>
    <link rel="stylesheet" href='<?php echo get_template_directory_uri(); ?>/assets/css/main.css' type='text/css'/>
</head>
<body id="top">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WVJ4SHX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 logo-top">
				<a href="#top">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/imgs/logo.svg" alt="logo proyecto">
				</a>
			</div>
			<div class="col-sm-9">
				<ul class="menu-top">
					<li class="hightlight hidden-xs"><a href="#simulador-credito-home">Cotiza aquí</a></li>
					<li><a href="#beneficios">Proyecto</a></li>
					<li class="hightlight"><a href="#plantas">Plantas</a></li>
					<li><a href="#conoce2">¿Por qué comprar?</a></li>
					<li class="hidden-xs"><a href="#contacto">Ubicación</a></li>
					<li><a href="#contacto">Contáctanos</a></li>
				</ul>
			</div>
		</div>
	</div>
</header>